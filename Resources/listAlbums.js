var win = Ti.UI.currentWindow;


var albumTableData = [];
var albumTableView = Ti.UI.createTableView({
	
});

albumTableView.addEventListener("click", function(e) {
	
	// Skapa fönster och skicka med albumid 
	photosWin = Ti.UI.createWindow({
		url: "listPhotos.js",
	    title:e.rowData.albumname,
	    backgroundColor:'#fff',
	    albumid: e.rowData.albumid		
	});
	Ti.UI.currentTab.open(photosWin);
});
win.add(albumTableView);

function makeTableRow(albumid, albumname)
{
	row = Ti.UI.createTableViewRow({
		height: "50dp",
		albumid: albumid,
		albumname: albumname
	});
	
	albumLabel = Ti.UI.createLabel({
		text: albumname,
		left: "5dp"
	});
	row.add(albumLabel);
	
	albumTableData.push(row);
}






var fb = require('facebook');
fb.appid = "306800102792885";

// Hämta inloggad användares album
fb.requestWithGraphPath('me/albums', {}, 'GET', function(e) {
    if (e.success) {
        
        fbJson = JSON.parse(e.result);
		
		for(i = 0;i < fbJson.data.length;i++)
		{
			makeTableRow(fbJson.data[i].id, fbJson.data[i].name);
			Ti.API.info(fbJson.data[i].name);
		}
		albumTableView.setData(albumTableData);
    } else if (e.error) {
        alert(e.error);
    } else {
        alert('Unknown response');
    }
});

