var win = Ti.UI.currentWindow;

// Ladda facebook modul
var fb = require('facebook');
// Ändra ill din facebook apps id
fb.appid = "306800102792885";

// Betyder visa dialogruta i webvy. Ändra till false för test på telefon.
fb.forceDialogAuth = true;
fb.permissions = ["user_photos"];


var profileImage = Ti.UI.createImageView({
	height: "100dp",
	top: "30dp",
	visible: false
});

win.add(profileImage);

// Funktion för att sätta användarens profilbild. Körs efter inloggning.
function setProfileImage()
{
	fb.requestWithGraphPath("/me", {}, 'GET', function(e) {
    if (e.success) {
        
        fbJson = JSON.parse(e.result);
		
		profileImage.image = "https://graph.facebook.com/"+fbJson.id+"/picture";
		profileImage.show();
				
    } else if (e.error) {
        alert(e.error);
    } else {
        alert('Unknown response');
    }
});
}

var loginToFacebookButton = Ti.UI.createButton({
	title: "Logga in",
	width: "100dp",
	height: "50dp"
});

loginToFacebookButton.addEventListener("click", function(e) {
	fb.authorize();
});

win.add(loginToFacebookButton);

var gotoAlbums = Ti.UI.createButton({
	title: "Visa Album",
	width: "100dp",
	height: "50dp"
});

gotoAlbums.addEventListener("click", function(e) {
	albumWin = Ti.UI.createWindow({
		url: "listAlbums.js",
	    title:'Albums',
	    backgroundColor:'#fff'		
	});
	Ti.UI.currentTab.open(albumWin);
});

win.add(gotoAlbums);

if(fb.loggedIn)
{
	setProfileImage();
	gotoAlbums.show();
	loginToFacebookButton.hide();
} else {
	gotoAlbums.hide();
	loginToFacebookButton.show();	
}

fb.addEventListener('login', function(e) {
    if (e.success) {
    	setProfileImage();
		gotoAlbums.show();
		loginToFacebookButton.hide();        
    } else if (e.error) {
        alert(e.error);
    } else if (e.cancelled) {
        alert("Canceled");
    }
});


